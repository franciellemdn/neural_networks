'''
  @Arquivo   : back_propagation.py
  @Descrição : Arquivo reune funções que implementação o algoritmo
               de backprogation.

  @Plataforma: Python 3.6 executando em Ubuntu 14.04 Trust

  @Autoras: Carolina Oltramary
            Francielle Marques
            Vanessa Borba de Souza
'''
'''Bibliotecas para manipulacao de dados (pd) e tipos (np)'''

import sys
import numpy as np
import utils
import bp_functions as bp

if __name__ == "__main__":
    # Formata impressao de matriz para 5 casas decimais
    np.set_printoptions(precision=5)

    arq_network = ""
    arq_initial_weightsCamadas = ""
    nameDataset = ""

    utils.VERBOSE = 1
    bp.K_FOLDS = 10

    if (len(sys.argv) > 3):
        arq_network = sys.argv[1]
        arq_initial_weightsCamadas = sys.argv[2]
        nameDataset = sys.argv[3]
    else:
        print("\nExecute: python back_propagation.py network1.txt initial_weights1.txt dataset1.txt")
        print("\nIMPORTANTE: o algoritmo gerado espera que a coluna atributo alvo seja a ultima coluna do dataset!")
        print("\nRede1: python3 back_propagation.py network1.txt initial_weights1.txt dataset1.txt")
        print("\nRede2: python3 back_propagation.py network2.txt initial_weights2.txt dataset2.txt")

    if arq_network != "" and arq_initial_weightsCamadas != "" and nameDataset != "":
        matGradBP = bp.Benchmark(arq_network, arq_initial_weightsCamadas, nameDataset)
    else:
        print("\nRevise os parâmetros informados!")
