'''
  @Arquivo   : utils.py
  @Descrição : Arquivo reune funções auxiliares usadas para calculo
               entre matrizes.

  @Plataforma: Python 3.6 executando em Ubuntu 14.04 Trust

  @Autoras: Carolina Oltramary
            Francielle Marques
            Vanessa Borba de Souza
'''
import pandas as pd
import numpy as np
from numpy import linalg as npla

'''Constantes do Sistema'''
VERBOSE = 0

'''
  @Function		ProductHadamard
  @Brief		Funcao que executa multiplicacao entre matrizes via produto Hadamard
                no qual o resultado e um vetor
  @Input Parameters
  @param   		x - primeira lista de valores
  @param        y - segunda lista de valores
  @return		retorna lista resultante da multiplicacao ponto a ponto
'''
def ProductHadamard(x, y):
    x = np.array(x)
    y = np.array(y)
    return (x * y)

'''
  @Function		CopiaLista
  @Brief		Funcao que efetua copia real entre listas, evitando problema python de apontamento 
  @Input Parameters
  @param   		lista - dados de origem
  @return		Novo endereco de memoria para onde dados foram copiados
'''
def CopiaLista(lista):
    cp = np.copy(lista).tolist()
    return cp

'''
  @Function		SomaMatriz
  @Brief		Funcao para somar duas matrizes

  @Input Parameters

  @param   		mat1 - Matriz 1
        		mat2 - Matriz 2
  @return       Matriz resultante da soma das matrizes 1 e 2
'''
def SomaMatriz(mat1, mat2):
    somaTotal = []
    for i in range(len(mat1)):
        aux = []
        aux.append(mat1[i])
        aux.append(mat2[i])
        resp = np.sum(aux, axis=0).tolist()
        somaTotal.append(resp)
    return somaTotal

'''
  @Function		SubtraiMatriz
  @Brief		Funcao para subtrair duas matrizes

  @Input Parameters

  @param   		mat1 - Matriz 1
        		mat2 - Matriz 2
  @return       Matriz resultante da subtracao das matrizes 1 e 2
'''
def SubtraiMatriz(mat1, mat2):
    subTotal = []
    for i in range(len(mat1)):
        resp = np.subtract(mat1[i], mat2[i]).tolist()
        subTotal.append(resp)
    return subTotal

'''
  @Function		DivideMatriz
  @Brief		Funcao para dividir duas matrizes

  @Input Parameters

  @param   		mat1 - Matriz 1
        		mat2 - Matriz 2
  @return       Matriz resultante da divisao da matriz 1 pela matriz 2
'''
def DivideMatriz(mat1, mat2):
    subTotal = []
    for i in range(len(mat1)):
        resp = np.divide(mat1[i], mat2[i]).tolist()
        subTotal.append(resp)
    return subTotal

'''
  @Function		Normalization
  @Brief		Funcao que realiza normalizacao da lista de valores.

  @Input Parameters

  @param   		list - lista a ser normalizada

  @return       Lista normalizada
'''
def Normalization(list):
    subTotal = []
    min = np.min(list)
    max = np.max(list)
    for i in range(len(list)):
        normValue = (list[i] - min) / (max - min)
        subTotal.append(normValue)

    return subTotal

'''
  @Function		Norma
  @Brief		Funcao calcula a norma para cada vetor de valores
                da matriz informada

  @Input Parameters

  @param   		mat - Matriz de valores
  @return       Lista com a norma para cada vetor
'''
def Norma(mat):
    lstNorma = []
    for i in range(len(mat)):
        resp = npla.norm(mat[i]).tolist()
        lstNorma.append(resp)
    return lstNorma

'''
  @Function		ConfweightsCamadas
  @Brief		Funcao que processa os pesos conforme especificacao e a transforma em dados 
  @Input Parameters
  @param   		initial_weightsCamadas
  @return		retorna uma lista de weightsCamadas, onde cada posicao da lista, 
  possui os respectivos neuronios das camadas e os pesos
'''
def ConfweightsCamadas(initial_weightsCamadas):
    weightsCamadas = []

    for index, value in initial_weightsCamadas.iterrows():
        aux = []
        tmp = value[0].split(';')
        for i in tmp:
            lst = i.split(',')
            for j in range(len(lst)):
                lst[j] = round(float(lst[j]), 4)
            aux.append(lst)
        weightsCamadas.append(aux)
    return weightsCamadas

'''
  @Function		ConfEntrada
  @Brief		Funcao que processa a entrada conforme especificacao e a transforma em dados 
  @Input Parameters
  @param   		dataset
  @return		retorna uma lista nesse estilo:
                       X              Y              X              Y
                [[[0.32, 0.68], [0.75, 0.98]], [[0.83, 0.02], [0.75, 0.28]]]
'''
def ConfEntrada(dataset):
    xy = []
    for index, value in dataset.iterrows():
        temp = value[0].split(';')
        aux = []
        for i in temp:
            lst = i.split(',')
            for j in range(len(lst)):
                lst[j] = round(float(lst[j]), 4)
            aux.append(lst)
        xy.append(aux)
    return xy

'''
  @Function		ImprimeInfors
  @Brief		Funcao que decide se ira fazer log do sistema ou nao
  @Input Parameters
  @param   		*args - vetor de args
  @return		None
'''
def ImprimeInfors(*args):
    if VERBOSE > 0:
        print(*args)

'''
  @Function		ConfweightsCamadas
  @Brief		Funcao que processa os pesos conforme especificacao e a transforma em dados 
  @Input Parameters
  @param   		initial_weightsCamadas
  @return		retorna uma lista de weightsCamadas, onde cada posicao da lista, 
  possui os respectivos neuronios das camadas e os pesos
'''
def ConfweightsCamadas(initial_weightsCamadas):
    weightsCamadas = []

    for index, value in initial_weightsCamadas.iterrows():
        aux = []
        tmp = value[0].split(';')
        for i in tmp:
            lst = i.split(',')
            for j in range(len(lst)):
                lst[j] = round(float(lst[j]), 4)
            aux.append(lst)
        weightsCamadas.append(aux)
    return weightsCamadas

'''
  @Function		ConfEntrada
  @Brief		Funcao que processa a entrada conforme especificacao e a transforma em dados 
  @Input Parameters
  @param   		dataset
  @return		retorna uma lista nesse estilo:
                       X              Y              X              Y
                [[[0.32, 0.68], [0.75, 0.98]], [[0.83, 0.02], [0.75, 0.28]]]
'''
def ConfEntrada(dataset):
    xy = []
    for index, value in dataset.iterrows():
        temp = value[0].split(';')
        aux = []
        for i in temp:
            lst = i.split(',')
            for j in range(len(lst)):
                lst[j] = round(float(lst[j]), 4)
            aux.append(lst)
        xy.append(aux)
    return xy

'''
  @Function	GeraArquivoSaida
  @Brief		Função que gera arquivo com saída conforme solicitado na
                especificação do trabalho.
  @Input Parameters

  @param   		gradRegular - matriz de gradientes para impressao
  @return		None
'''
def GeraArquivoSaida(gradRegular):
    f = open('final', 'w')
    for i in gradRegular:
        # print('i', i)
        for j in i:
            if (j != i[0]):
                f.write(';')
            # print(';', end=" ")
            for k in range(len(j)):
                f.write(str(j[k]))
                # print(j[k],end=" ")
                if (len(j) - 1 != k):
                    f.write(',')
                    # print(',', end=" ")
        # print(';', end= " ")
        f.write(';')
        # print(i[-1])
        f.write('\n')
        # print('\n')
        # f.write(i)
    f.close()

    with open("final", 'r') as file, \
            open('final-weights', 'w') as outfile:
        index = -2
        char = " "
        for l in file:
            word2 = l[:index] + char
            # print('word2', word2)
            outfile.write(word2)
            outfile.write('\n')

'''
  @Function		CarregaDados
  @Brief		Funcao que carrega os dados dos arquivos especificados e padroniza no formato
                esperado pelo BackPropagation

  @Input Parameters

  @param   	    arq_network: caminho para carregar arquivo de configuração da rede
                arq_initial_weightsCamadas: caminho para carregar arquivo contendo pesos iniciais da rede
                nameDataset: caminho do arquivo dataset

  @return       LBDA,xy,weightsCamadas,rede
'''
def CarregaDados(arq_network, arq_initial_weightsCamadas, nameDataset):
    conf_network = pd.read_csv(arq_network, delimiter='\n', names=['rede'])
    LBDA = conf_network['rede'][0]
    initial_weightsCamadas = pd.read_csv(arq_initial_weightsCamadas, delimiter='\n', names=['pesos'])
    dataset = pd.read_csv(nameDataset, delimiter='\n', names=['conjunto'])
    xy = ConfEntrada(dataset)
    weightsCamadas = ConfweightsCamadas(initial_weightsCamadas)
    rede = conf_network['rede'].tolist()

    return LBDA, xy, weightsCamadas, rede
