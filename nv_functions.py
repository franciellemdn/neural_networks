'''
  @Arquivo   : numerical_verification.py
  @Descrição : Arquivo contém funções que implementam a verificação
               numérica dos gradientes conforme dados passados
               na chamada do programa.

  @Plataforma: Python 3.6 executando em Ubuntu 14.04 Trust

  @Autoras: Carolina Oltramary
            Francielle Marques
            Vanessa Borba de Souza
'''
'''Bibliotecas para manipulacao de dados (pd) e tipos (np)'''

import numpy as np
import utils
import bp_functions as bp

'''Constantes do Sistema'''
EPSILON = 0.000001000

'''
  @Function		NumericalGradientChecking
  @Brief		Funcao realiza verificacao numerica para gradientes.
                Varia o valor de cada theta em theta+epsilon e theta-epsilon.
                Aplica entrada e matriz de peso com theta modificado para obter
                ftheta(x). Finalmente, aplica a funcao que considera o custo e
                epsilon para estimar o valor do gradiente descendente para o theta

  @Input Parameters

  @param   		weightsCamadas - Matriz de pesos das camadas
        		epsilon - Valor epsilon para variacao

  @return       None
'''
def NumericalGradientChecking(xy, weightsCamadas, epsilon):
    n = len(xy)
    weights = utils.CopiaLista(weightsCamadas)
    lstFinal = []
    lstGradsAux = []
    matD = []

    for iReg in range(len(xy)):
        x = xy[iReg][:-1][0]
        y = xy[iReg][-1:][0]
        lstGradsAux = []
        for iWC in range(len(weights)):
            utils.ImprimeInfors("Camada ", weights[iWC])
            lstGradientes = []
            for iThetas in range(len(weights[iWC])):
                utils.ImprimeInfors("Thetas ", weights[iWC][iThetas])
                thetas = weights[iWC][iThetas]
                lstGradientesNP = []
                for i in range(len(thetas)):
                    utils.ImprimeInfors("Theta ", weights[iWC][iThetas][i])

                    thetaOriginal = weights[iWC][iThetas][i]
                    weights[iWC][iThetas][i] = thetaOriginal + epsilon
                    fxP = bp.Propagation(x, weights)
                    utils.ImprimeInfors("fxP ", fxP)

                    weights[iWC][iThetas][i] = thetaOriginal - epsilon
                    bp.RemoveBias(x)
                    fxN = bp.Propagation(x, weights)
                    utils.ImprimeInfors("fxN ", fxN)

                    bp.RemoveBias(x)
                    weights[iWC][iThetas][i] = thetaOriginal

                    j = (bp.FuncaoCusto(y, fxP[len(fxP) - 1]) - bp.FuncaoCusto(y, fxN[len(fxN) - 1])) / (2 * epsilon)
                    utils.ImprimeInfors("Valor J: ", "{:.5f}".format(j))
                    lstGradientesNP.append(j)

                lstGradientes.append(np.transpose(lstGradientesNP))
            lstGradsAux.append(np.transpose(lstGradientes))
            lstFinal.append(np.transpose(lstGradientes))

        if (iReg == 0):
            matD = lstGradsAux
        else:
            matD = utils.SomaMatriz(lstGradsAux, matD)

    matP = bp.GeraP(weightsCamadas)
    matPD = bp.SomaPD(matP, matD)
    gradientesF = bp.GradienteRegularizado(n, matPD)

    print('\n========== VERIFICACAO NUMERICA =============')
    print("Estimativa para os Gradientes:")

    bp.ImprimeGradiente(gradientesF,-2)

    return gradientesF

'''
  @Function		CorretudeGradientes
  @Brief		Funcao analisa a corretude dos gradientes calculados
                pelo backpropagation analizando o erro em relacao a 
                aos gradientes estimados pela verificacao numerica, segundo a 
                formula: norm(gradients - numericalGradients)/norm(gradients + numericalGradients)

  @Input Parameters

  @param   	    matGradBP - Matriz gradiente calculada pelo backpropagation
                matGradNum - Matriz gradiente calculada pela verificacao numerica
                
  @return       None
'''
def CorretudeGradientes(matGradBP, matGradNum):
    sub = utils.SubtraiMatriz(matGradNum, matGradBP)
    sum = utils.SomaMatriz(matGradNum, matGradBP)
    normSub = utils.Norma(sub)
    normSum = utils.Norma(sum)

    matRes = utils.DivideMatriz(normSub, normSum)

    print('\n========== ANALISE DE CORRETUDE DOS GRADIENTES =============')
    for k in range(len(matRes)):
        print("\nGradientes para Theta " + str(k + 1))
        print(np.transpose(matRes[k]))
