'''
    Programa de Pos-Graduacao em Computacao - UFRGS

    Trabalho 1: Implementar o algoritmo de redes neurais para tarefas
                de classificacao seguindo a especificacoes do trabalho

    Plataforma: Python 3.6 executando em Ubuntu 14.04 Trust

    Autoras: Carolina Oltramary
             Francielle Marques
             Vanessa Borba de Souza

'''

'''Bibliotecas para manipulacao de dados (pd) e tipos (np)'''

import sys
import pandas as pd
import numpy as np
import random as rd
import process_dataset as dat
import utils
import bp_functions as bp

'''Constantes do Sistema'''
SEED = 5
IMPRIME_CURVA_J_TESTE = 0

'''
  @Function		ReadConfRede
  @Brief		Funcao configurar treinamento com dados vindo dos arquivos
                de pesos iniciais e config da rede.

  @Input Parameters

  @param   	    arq_network - arquivo com arquitetura da rede
                arq_initial_weightsCamadas - arquivo contendo os pesos das camadas
                flagImpr - Para impressão dados da rede

  @return       None
'''
def ReadConfRede(arq_network,arq_initial_weightsCamadas,flagImpr):
    conf_network = pd.read_csv(arq_network, delimiter='\n', names=['rede'])
    bp.LBDA = conf_network['rede'][0]
    initial_weightsCamadas = pd.read_csv(arq_initial_weightsCamadas, delimiter='\n', names=['pesos'])
    weightsCamadas = utils.ConfweightsCamadas(initial_weightsCamadas)
    rede = conf_network['rede'].tolist()

    if (flagImpr == 0):
        print('\nEstrutura da Rede: ', rede)
        print('Alfa: ', str(round(bp.ALFA, 5)))
        print('Lambda: ', bp.LBDA)
        print('Beta Momento: ', bp.BETA_MOMENTO)
        print('K-Folds: ', dat.K_FOLDS)
        print('Seed: ', SEED)
        print('\n')

    return weightsCamadas

'''
  @Function		ImprimePerformanceRede
  @Brief		Funcao para imprimir o custo J da rede a cada 
                n exemplos de treinamentos apresentados a rede.
                Para tanto, acumula a lista de pesos gerada a cada batch

  @Input Parameters

  @param   	    xyTest - Conjunto de treinamento
                lenBatch - tamanho do batch de treinamento

  @return       None
'''
def ImprimePerformanceRede(xyTestJ, lenBatch):

    print('\nIteração;TotalAmostras;JConjuntoTeste')
    totalSamples = 0
    qtLote = (len(bp.gLstWeigth)/bp.MAX_TRY_BP)
    count = 0
    it = 1
    print('\nIteracao:', it) #iteracao aqui e a quantidade de vezes que a rede rodou com os exemplos
    #se MAX_TRY_BP = 5, entao, houve 5 iteracoes para cada fold...
    for iGW in range(len(bp.gLstWeigth)):
        lstWeigth = bp.gLstWeigth[iGW]
        jRede = []
        if(count == qtLote):
            count = 0
            it = it + 1
            print('Iteracao:', it)


        totalSamples = totalSamples + lenBatch
        for i in range(len(xyTestJ)):
            x = xyTestJ[i][:-1][0][1:]
            y = xyTestJ[i][-1:][0]
            lstAtivacao = bp.Propagation(x, lstWeigth)
            j = bp.FuncaoCusto(y, lstAtivacao[len(lstAtivacao) - 1])

            jRede.append(j)
        J = np.mean(jRede)
        # print('J total do dataset (com regularizacao):', J + s)
        print(str(iGW+1),str(totalSamples), "{:.10f}".format(J))
        count = count + 1

'''
  @Function		AvaliaPerformance
  @Brief		Funcao que realiza todo o procedimento do algoritmo. Desde o treinamento,
                ate a avaliacao utilizando o metodo de validacao cruzada

  @Input Parameters

  @param   	    arq_network - passa arquivo com configuração da rede
                arq_initial_weightsCamadas - passa arquivo com pesos da rede
                layerWeight - Caso não informa arquivo, pode informar vetor da topologia da rede
                nameDataset - Referencia para arquivo do conjunto de dados
                treinamento - tipo de treinamento selecionado pelo usuario

  @return       Resultados das metricas
'''
def AvaliaPerformance(arq_network, arq_initial_weightsCamadas, layerWeight, nameDataset, treinamento):
    print('=========================================')
    print('\nAVALIACAO DE PERFORMANCE')
    dataset = pd.read_csv(nameDataset, delimiter=';')
    conjuntos = dat.ValidacaoCruzada(dataset)
    print('Dataset:', nameDataset)
    lstRedes = []
    recallS = []
    precisionS = []
    fMeasureS = []
    acuraciaS = []

    flagImpr = 0
    cont = 1
    for train, test in conjuntos:
        flagWeights = 0
        y_pred = []
        y_true = []
        for xyTrain, xyTest in zip(train, test):
            if (flagWeights == 0):    # configura a rede... com base na entrada e saida
                qtNeuronEntrada = len(xyTrain[0][:-1][0])
                qtNeuronSaida = len(xyTrain[0][-1:][0])
                flagWeights = 1

                if layerWeight == None:
                    weightsCamadas = ReadConfRede(arq_network, arq_initial_weightsCamadas, flagWeights);
                else:
                    weightsCamadas = bp.GeraConfRedeRandom(qtNeuronEntrada, layerWeight, qtNeuronSaida,flagImpr)

                flagImpr = 1
                treinamento = bp.TipoTreinamento(len(dataset), len(xyTrain), treinamento)

            # realiza o treino da rede
            weightsCamadas = bp.BackPropagation(weightsCamadas, xyTrain, treinamento)

            utils.ImprimeInfors("REALIZANDO AS PREVISOES DOS EXEMPLOS TESTES")

            for i in range(len(xyTest)):
                x = xyTest[i][:-1][0]
                y = xyTest[i][-1:][0]
                lstAtivacao = bp.Propagation(x, weightsCamadas)
                y_pred.append(lstAtivacao[len(lstAtivacao) - 1])
                y_true.append(y)
                utils.ImprimeInfors('\nSaida predita para o exemplo ' + str(i + 1) + ':',
                                            ["{:.5f}".format(value) for value in lstAtivacao[len(lstAtivacao) - 1]])
                utils.ImprimeInfors('Saida esperada para o exemplo ' + str(i + 1) + ':', ["{:.5f}".format(value) for value in y])

                if (y_true and y_pred):
                    recall, precision, fMeasure, acuracia = dat.MetricasDesempenho(y_true, y_pred)
                    recallS.append(recall)
                    precisionS.append(precision)
                    fMeasureS.append(fMeasure)
                    acuraciaS.append(acuracia)
                    lstRedes.append([weightsCamadas, fMeasure])


            if (IMPRIME_CURVA_J_TESTE == 1):
                print('\nFOLD ',cont)
                cont = cont + 1
                xy = utils.CopiaLista(xyTest)
                ImprimePerformanceRede(xy, treinamento)
                bp.gLstWeigth = []


    stdA = [np.std(recallS), np.std(precisionS), np.std(fMeasureS), np.std(acuraciaS)]
    meanA = [np.mean(recallS), np.mean(precisionS), np.mean(fMeasureS), np.mean(acuraciaS)]
    result = pd.DataFrame(dict(std=stdA, mean=meanA), index=['Recall', 'Precision', 'fMeasure', 'Acuracia'])
    print(result)
    print('=========================================')
    return result


if __name__ == "__main__":
    # Formata impressao de matriz para 5 casas decimais
    np.set_printoptions(precision=5)

    arq_network = ""
    arq_initial_weightsCamadas = ""
    nameDataset = ""

    if (len(sys.argv) > 1):
        nameDataset = sys.argv[1]
        if (len(sys.argv) > 2):
            arq_network = sys.argv[2]
        if (len(sys.argv) > 3):
            arq_initial_weightsCamadas = sys.argv[3]
        if (len(sys.argv) > 4):
            bp.TREINAMENTO = int(sys.argv[4])

    else:
        print("\nExecute: python3 neural_network.py nomeArquivoDataset.csv network1.txt initial_weights1.txt [tipoTreinamento]")
        print("\nOnde:\nnomeArquivoDataset.csv = recebe o nome do arquivo que contem o dataset")
        print("\nconf_rede.txt = estrutura da rede")
        print("\npesos.txt = arquivo indicando os pesos iniciais")
        print("\ntipoTreinamento = informe:" +
                    "\n-> 1 para treintamento estocástico, " +
                    "\n-> total de registros do conjunto de dados para treinamento batch ou " +
                    "\n-> outro valor positivo entre 1 e total de registros do conjunto de dados " +
                    "\n     para treinamento mini-batch. Se valor não informado ou inválido," +
                    "\n     assume padrão mini-batch com lote de tamanho igual a 25% do total de amostras do conjunto de dados.")
        print("\nIMPORTANTE: o algoritmo gerado espera que a coluna atributo alvo seja a ultima coluna do dataset!")

    # Parâmetros adicionais para configuração da rede
    SEED = 5
    rd.seed(SEED)
    IMPRIME_CURVA_J_TESTE = 1
    utils.VERBOSE = 0
    layerWeight = None

    # bp.MAX_TRY_BP = 10000
    # bp.TREINAMENTO = 0
    # bp.CUSTO_ACEITAVEL = 0.1
    # bp.VAR_CUSTO_ACEITAVEL = 0.1
    # bp.BETA_MOMENTO = 0.9
    # bp.ALFA = 0.01
    # bp.LBDA = 0.250
    # dat.K_FOLDS = 10
    # layerWeight = [2,2]

    # nameDataset = 'datasets/wine_data.csv'
    # nameDataset = 'datasets/ionosphere_data.csv'
    # nameDataset = 'datasets/wdbc_data.csv'
    # nameDataset = 'datasets/irisDataset.csv'
    # nameDataset = 'datasets/pima-indians-diabetes.csv'

    import time

    if (arq_network != "" and arq_initial_weightsCamadas != "" and nameDataset != "") or nameDataset != "":
        ini = time.time()
        AvaliaPerformance(arq_network, arq_initial_weightsCamadas, layerWeight, nameDataset, bp.TREINAMENTO)
        fim = time.time()
        print("Tempo de Execução:", fim-ini)
    else:
        print("\nRevise os parâmetros informados!")
