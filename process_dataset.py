'''
  Programa de Pos-Graduacao em Computacao - UFRGS

  Trabalho 1: Implementar o algoritmo de redes neurais para tarefas
              de classificacao seguindo a especificacoes do trabalho

  Plataforma: Python 3.6 executando em Ubuntu 14.04 Trust

  Autoras: Carolina Oltramary
          Francielle Marques
          Vanessa Borba de Souza

'''

'''Bibliotecas para manipulacao de dados (pd) e tipos (np)'''

import pandas as pd
import numpy as np
import random as rd
from sklearn.metrics import f1_score, recall_score, precision_score, accuracy_score, r2_score, mean_squared_error
import warnings
warnings.filterwarnings('ignore')  # "error", "ignore", "always", "default", "module" or "once"
'''Constantes do Sistema'''
K_FOLDS = 10


'''A validacao_cruzada_estratificada e uma variacao de k-fold que retorna dobras estratificadas: 
   cada conjunto contem aproximadamente a mesma porcentagem de amostras de cada classe alvo que 
   o conjunto completo.'''

'''
  @Function		GeraFolds

  @Brief		Funcao que separa a lista de elementos em grupos (folds) pela quantidade de partes/split

  @Input Parameters

  @param   		lista - lista com os valores gerados randomicamente indicando o indice da linha/row do dataset
  @param   		partes - quantidade de partes que tera os folds

  @return		retorna uma lista de resultados/folds 
'''
def GeraFolds(lista, partes):
    splited = []
    len_l = len(lista)
    for i in range(partes):
        start = int(i * len_l / partes)
        end = int((i + 1) * len_l / partes)
        splited.append(lista[start:end])
    # print(splited)
    return splited


'''
  @Function		GeraAleatorio

  @Brief		Funcao que gera uma lista com valores aleatorios 
  @Input Parameters

  @param   		tam - tamanho da lista que devera ser gerada, corresponde ao tamanho da group.dataset analisado
  @param   		reposicao - True, para amostragem com reposicao
  @return		retorna uma lista de numeros aleatorios dentro de um range 
'''
def GeraAleatorio(tam, reposicao):
    result = []
    while len(result) < tam:
        r = rd.randint(0, tam - 1)
        if reposicao == True:
            result.append(r)
        else:
            if r not in result:
                result.append(r)

    return result

'''
  @Function		GeraDataFolds

  @Brief		Funcao que gera uma lista de datasets com os folds
  @Input Parameters

  @param   		dataset - grupo/dataset que esta sendo analisado
  @param        lstFolds - uma lista de resultados/folds
  @return		retorna uma lista contendo os dados dos folds
'''
def GeraDataFolds(dataset, lstFolds):
    lstDataset = []
    for i in lstFolds:
        lstDataset.append(dataset.iloc[i])
    return lstDataset


'''
  @Function		GeraTrainTest

  @Brief		Funcao que gera os conjuntos de treino e teste com base na validacao_cruzada
  @Input Parameters

  @param   		folds -  conjunto de folds 
  @return		Uma lista contendo dados para treino e teste (se K_FOLDS for 3, havera 3 conjuntos treino e teste)
'''
def GeraTrainTest(folds):
    conjuntos = []
    test = []
    for i in range(len(folds)):
        frames = []
        for j in range(len(folds)):
            if i == j:  # define o que ira ficar para teste
                test = folds[i]
                test.fillna(0, inplace=True)
            else:
                frames.append(folds[j])  # define o que ira ficar para treino

        # concatena
        train = pd.concat(frames, ignore_index=True)
        train.fillna(0, inplace=True)
        # armazena cada conjunto de treino e teste em uma lista kk
        conjuntos.append([train, test])  # lista com dois dataframes
        # df = pd.DataFrame.from_records([train,test], columns=['Train', 'Test'])
        # conjuntos.append(df)
    return conjuntos

'''
  @Function		EstratificaFolds

  @Brief		Funcao que executa o processo de estratificar os dados em folds 
  @Input Parameters

  @param   		dataset - dataset principal
  @return		no futuro saberemos
'''
def EstratificaFolds(dataset):
    atrAlvo = dataset.columns[len(dataset.columns) - 1]
    lstDataFolds = []
    for name, group in dataset.groupby(atrAlvo):
        # print(name)
        # print(group)
        result = GeraAleatorio(group.shape[0], False)
        # print (result)
        lstFolds = GeraFolds(result, K_FOLDS)
        # print (lstFolds)
        lstDataFolds.append(GeraDataFolds(group, lstFolds))

    folds = []
    for i in range(K_FOLDS):
        frames = []
        for j in lstDataFolds:
            # print j[i]
            frames.append(j[i])
        data = pd.concat(frames, ignore_index=True)
        classe = data['class']
        # print('data', data)
        data = data.drop(['class'], axis=1)
        dataf = ((data - data.min()) / (data.max() - data.min()))
        dataf['class'] = classe
        # print('data', dataf)
        folds.append(dataf)
    return folds

'''
  @Function		MetricasDesempenho

  @Brief		Funcao que realizara a avaliacao da arvore a partir do conjunto de teste.
                Passo 1: Realizar o test e computar os acertos e erros
                Passo 2: Realizar a F-Measure para avaliar
  @Input Parameters

  @param   		tree - arvore gerada
  @param        test - conjunto de teste
  @return		no futuro saberemos
'''
def MetricasDesempenho(y_true, y_pred):
    recallS = []
    precisionS = []
    fMeasureS = []
    acuraciaS = []

    for i in range(len(y_pred)):
        maior = max(y_pred[i])
        for j in range(len(y_pred[i])):
            if (y_pred[i][j] == maior):
                y_pred[i][j] = 1
            else:
                y_pred[i][j] = 0
    nTrue = []
    nPred = []

    # Decodifica para computar as metricas
    for i in range(len(y_true)):
        nPred.append(OneHotDecoding(y_pred[i]))
        nTrue.append(OneHotDecoding(y_true[i]))

    fMeasureS.append(f1_score(nTrue, nPred, average=None))
    recallS.append(recall_score(nTrue, nPred, average=None))
    precisionS.append(precision_score(nTrue, nPred, average=None))
    acuraciaS.append(accuracy_score(nTrue, nPred))

    # print("A FMeasure e:",fMeasure)
    recall = np.mean(recallS)
    precision = np.mean(precisionS)
    fMeasure = np.mean(fMeasureS)
    acuracia = np.mean(acuraciaS)

    return recall, precision, fMeasure, acuracia

'''
  @Function		ValidacaoCruzada

  @Brief		Funcao que realizara todo o processo de validacao_cruzada.
                Passo 1: Gerar folds
                Passo 2: Gerar todas as combinacoes de conjuntos train e test
                Passo 3: Reorganiza os dados conforme esperado pela função backpropagation
  @Input Parameters

  @param   		dataset - dataset principal
  @return		no futuro saberemos
'''
def ValidacaoCruzada(dataset):
    folds = EstratificaFolds(dataset)
    conjuntosA = GeraTrainTest(folds)
    totClasses = len(dataset["class"].value_counts())
    conjuntos = ReorganizaDados(conjuntosA, totClasses)
    return conjuntos

'''
  @Function		OneHotEncoding
  @Brief		Funcao que transforma Y em binario

  @Input Parameters

  @param   		y - valor de y (0,1,2)
                totClasses - total de classes do dataset

  @return       Codificacao em binario para y 
                Exemplo, se y = 0, return [1,0] se totClasses = 2
'''
def OneHotEncoding(y, totClasses):
    lstEncoding = []
    for i in range(totClasses):
        aux = []
        for j in range(totClasses):
            aux.append(0)
        aux[i] = 1
        lstEncoding.append(aux)
    for i in range(totClasses):
        if y[0] == i:
            return lstEncoding[i]

'''
  @Function		OneHotDecoding
  @Brief		Funcao que transforma Y em binario

  @Input Parameters

  @param   		y - [0,0]
                totClasses - total de classes do dataset

  @return       Decodifica de binario para y 
                Exemplo, se y =[0,0] devolve 0
'''
def OneHotDecoding(y):
    for i in range(len(y)):
        e = OneHotEncoding([i], len(y))
        if (y == e):
            return i

'''
  @Function		ReorganizaDados
  @Brief		Funcao que reorganiza dados para estrutura esperada do BackPropagation

  @Input Parameters

  @param   		conjuntos - Conjuntos de Folds Train e Test

  @return       conjuntos de folds no padrao esperado pelo backpropagation (formato de listas de listas)
'''
def ReorganizaDados(conjuntos, totClasses):
    conjuntosFinal = []
    for train, test in conjuntos:
        lstTrain = train.values.tolist()
        lstTest = test.values.tolist()
        lstXYTrain = []
        lstXYTest = []
        trainFinal = []
        testFinal = []
        for i, j in zip(lstTrain, lstTest):
            lstXYTrain.append([i[:-1], OneHotEncoding(i[-1:], totClasses)])
            lstXYTest.append([j[:-1], OneHotEncoding(j[-1:], totClasses)])

        trainFinal.append(lstXYTrain)
        testFinal.append(lstXYTest)
        conjuntosFinal.append([trainFinal, testFinal])

    return conjuntosFinal
