'''
  @Arquivo   : bp_functions.py
  @Descrição : Contém funções que implementação o algoritmo
               de progation e backprogation.

  @Plataforma: Python 3.6 executando em Ubuntu 14.04 Trust

  @Autoras: Carolina Oltramary
            Francielle Marques
            Vanessa Borba de Souza
'''

'''Bibliotecas para manipulacao de dados (pd) e tipos (np)'''

import pandas as pd
import numpy as np
import math as mt
import random as rd
import process_dataset as dat
import utils
from neural_network import SEED

'''Constantes do Sistema'''
MAX_TRY_BP = 10000
TREINAMENTO = 0
CUSTO_ACEITAVEL = 0.1
VAR_CUSTO_ACEITAVEL = 0.1
BETA_MOMENTO = 0.9
ALFA = 0.01
LBDA = 0.250

'''Variáveis Globais'''
gLstWeigth = []

'''
  @Function	GeraArquivoSaida
  @Brief		Função que gera arquivo com saída conforme solicitado na
                especificação do trabalho.
  @Input Parameters
  
  @param   		gradRegular - matriz de gradientes para impressao
  @return		None
'''
def GeraArquivoSaida(gradRegular):
    f = open('final', 'w')
    for i in gradRegular:
        # print('i', i)
        for j in i:
            if (j != i[0]):
                f.write(';')
            # print(';', end=" ")
            for k in range(len(j)):
                f.write(str(j[k]))
                # print(j[k],end=" ")
                if (len(j)-1 != k):
                    f.write(',')
                    # print(',', end=" ")
        # print(';', end= " ")
        f.write(';')
        # print(i[-1])
        f.write('\n')
        # print('\n')
        #f.write(i)
    f.close()

    with open("final", 'r') as file, \
        open('final-weights', 'w') as outfile:
        index = -2
        char = " "
        for l in file:
            word2 = l[:index] + char
            # print('word2', word2)
            outfile.write(word2)
            outfile.write('\n')

'''
  @Function		AdicionaBias
  @Brief		Funcao que que insere na posicao 0 do vetor o elemento 1 referente ao bias
  @Input Parameters
  @param   		x - vetor de entradas
  @return		retorna a nova lista com o bias adicionado
'''
def AdicionaBias(x):
    if (type(x) is not list):
        x = [x]
    x.insert(0, 1)
    return x

'''
  @Function		RemoveBias
  @Brief		Funcao que que remove na posicao 0 do vetor o elemento 1 referente ao bias
  @Input Parameters
  @param   		x - vetor de entradas
  @return		retorna a nova lista com o bias removido
'''
def RemoveBias(x):
    if (type(x) is not list):
        x = [x]
    x.pop(0)
    return x

'''
  @Function		Sigmoid
  @Brief		Funcao que calcula o valor de ativacao do x
  @Input Parameters
  @param   		x - peso
  @return		retorna o novo valor, apos a ativacao
'''
def Sigmoid(x):
    return (1 / (1 + mt.exp(-x)))

'''
  @Function		AtivacaoNeuronio
  @Brief		Funcao que processa gera as saidas de ativacao para todos os neuronios
  @Input Parameters
  @param   		x - lista de pesos
  @return		retorna uma lista com os novos valores ativados
'''
def AtivacaoNeuronio(x):
    for i in range(len(x)):
        x[i] = Sigmoid(x[i])
    return x

'''
  @Function		Propagation
  @Brief		Funcao que propaga a entrada e gera a saida de um exemplo
  @Input Parameters
  @param   		x - lista de entrada
  @param        weightsCamadas - lista de camadas e os pesos em cada uma delas
  @return		retorna f(x) apos a propagacao
'''
def Propagation(x, weightsCamadas):
    lstAtivacao = []
    a = AdicionaBias(x)
    lstAtivacao.append(a)
    for i in range(len(weightsCamadas)):
        # utils.ImprimeInfors('\n=====CAMADA ' + str(i + 1) + '=====')
        utils.ImprimeInfors('\t a' + str(i + 1) + ':', ["{:.5f}".format(value) for value in a])
        matrixweightsCamadas = weightsCamadas[i]
        if (i == 0):
            utils.ImprimeInfors('\n')
        else:
            utils.ImprimeInfors('\n')
        # print('matrixweightsCamadas:', matrixweightsCamadas)
        z = np.matmul(matrixweightsCamadas, a)
        utils.ImprimeInfors('\t z' + str(i + 2) + ':', ["{:.5f}".format(value) for value in z])
        a = AtivacaoNeuronio(z.tolist())
        lstAtivacao.append(a)

        if (i == (len(weightsCamadas) - 1)):
            # print('\n=====CAMADA ' + str(i + 2) + '=====')
            utils.ImprimeInfors('\t a' + str(i + 2) + ':', ["{:.5f}".format(value) for value in a])
            utils.ImprimeInfors("\n\tf(x):", ["{:.5f}".format(value) for value in a])

            return lstAtivacao
        a = AdicionaBias(a)

'''
  @Function		CalculaGradientes
  @Brief		Funcao que calcula todos os gradientes para os thetas da rede

  @Input Parameters
  @param   		lstDelta - lista com valores de delta para todos os neuronios
  @param        lstAtivacao - lista com valores de ativacao para todos os neuronios
  @return		retorna lista com todos os gradientes das camadas
'''
def CalculaGradientes(lstDelta, lstAtivacao):
    lstAtivacao.reverse()
    lstDelta.reverse()
    lstGradientes = []
    # print("CALCULANDO GRADIENTES")
    for i in range(len(lstAtivacao) - 1):
        gradAux = []
        gradAux.append(lstDelta[i + 1])
        for j in range(len(lstAtivacao[i])):
            grad = utils.ProductHadamard(lstAtivacao[i][j], lstDelta[i + 1])
            gradAux.append(grad.tolist())
            # print(i,j)
            # print(grad)
        lstGradientes.append(gradAux)
    return lstGradientes

'''
  @Function		CalculaDelta
  @Brief		Funcao que calcula todos os deltas para os neuronios da rede
        
  @Input Parameters
  @param   		x 
  @param        y
  @return		retorna lista com todos os deltas das camadas
'''
def CalculaDelta(y, weightsCamadas, lstAtivacao):
    # lstAtivacao = Propagation(x, weightsCamadas)
    # print(x, y)
    lstDelta = []
    delta = np.subtract(lstAtivacao[len(lstAtivacao) - 1], y)
    lstDelta.append(delta.tolist())
    weightsCamadas.reverse()
    lstAtivacao.reverse()
    for i in range(len(weightsCamadas)):
        matrixweightsCamadas = np.transpose(weightsCamadas[i])
        parte1 = RemoveBias(matrixweightsCamadas.tolist())
        parte2 = RemoveBias(lstAtivacao[i + 1])
        parte3 = np.subtract(1, parte2)
        resul1 = np.matmul(parte1, delta)
        resul2 = utils.ProductHadamard(parte2, parte3)
        delta = utils.ProductHadamard(resul1, resul2)
        lstDelta.append(delta.tolist())

    k = len(lstDelta)
    for i in range(len(lstDelta) - 1):
        utils.ImprimeInfors("delta" + str(k) + ": ", ["{:.5f}".format(value) for value in lstDelta[i]])
        k = k - 1
    weightsCamadas.reverse()
    return lstDelta

'''
  @Function		Regularizacao
  @Brief		Funcao que faz a regularizacao do modelo

  @Input Parameters
  @param   		weightsCamadas - lista de thetas 
  @param        n - quantidade de exemplos
  @return		s - fator de regularizacao
'''
def Regularizacao(weightsCamadas, n):
    s = 0
    for i in range(len(weightsCamadas)):
        for j in (weightsCamadas[i]):
            ja = j[1:]    # faz uma copia...
            s = np.matmul(ja, ja) + s
    s = (LBDA / (2 * n)) * s
    return s

'''
  @Function		FuncaoCusto
  @Brief		Funcao que calcula o custo total da rede para treinamento

  @Input Parameters
  @param   		y - Saida esperada 
  @param        fx - Saida predita
  @return		Custo total da rede
'''
def FuncaoCusto(y, fx):
    # J (i) = -y (i) * log(f    (x (i) )) - (1-y (i) ) * log(1 - f (x (i) ))
    vetJ = []
    for i in range(len(fx)):
        j = -y[i] * np.log(fx[i]) - (1 - y[i]) * np.log(1 - fx[i])
        vetJ.append(j)
        # se tiver multiplas saidas calculamos o somatorio de J
        jTotal = j
        if (len(vetJ) > 1):
            jTotal = np.sum(vetJ)
    return jTotal

'''
  @Function		ImprimeGradiente
  @Brief		Funcao para exibir gradientes calculados
  
  @Input Parameters
  
  @param   		lstGradientes - Lista de Gradientes
        		i - Controle para exibir indice do exemplo
  @return       None
'''
def ImprimeGradiente(lstGradientes, i=-1):
    grad = utils.CopiaLista(lstGradientes)
    grad.reverse()
    k = len(grad)
    for j in grad:
        if (i == -1):
            utils.ImprimeInfors("\nGradientes Finais para Theta" + str(k) + " (com regularizacao):")
        elif (i == 5):
            print("\nTheta" + str(k) + " (pesos de cada neuronio, incluindo bias) ")
            print(np.transpose(j))
        elif(i == -2):
            print("\nGradientes numerico do Theta" + str(k))
            print(np.transpose(j))
        else:
            utils.ImprimeInfors("\nGradientes de Theta" + str(k) + " com base no exemplo " + str(i + 1))
        utils.ImprimeInfors(np.transpose(j))
        k = k - 1

'''
  @Function		GeraP
  @Brief		Funcao que aplica regularizacao lambda a matriz contendo os
                pesos de cada camada (P == lambda*matrizThetas, com a coluna bias zerado)

  @Input Parameters

  @param   		weightsCamadas - matriz de pesos, onde cada coluna eh o vetor
        		de pesos da camada correpondente ao indice
  @return       Matriz com os pesos atualizados
'''
def GeraP(weightsCamadas):
    p = utils.CopiaLista(weightsCamadas)
    for i in range(len(p)):
        p[i] = np.dot(p[i], LBDA).tolist()
        for j in range(len(p[i])):
            p[i][j][0] = 0
    return p

'''
  @Function		GradienteRegularizado
  @Brief		Funcao que aplica regularizacao aos gradientes

  @Input Parameters

  @param   		DP - matriz resultante, de D + P, onde: D - matriz de acumulado dos gradientes para cada exemplo
                                                        P - matriz lambda*matrizThetas
                n - total de instancias de treinamento
  @return       Matriz com os gradientes regularizados atualizados
'''
def GradienteRegularizado(n, DP):
    p = utils.CopiaLista(DP)
    for i in range(len(p)):
        for j in range(len(p[i])):
            for k in range(len(p[i][j])):
                p[i][j][k] = p[i][j][k] / n
    return p

'''
  @Function		SomaPD
  @Brief		Funcao auxiliar que soma a matriz D(gradientes)com a matriz de pesos

  @Input Parameters

  @param   		matP - Matriz com valores dos pesos calculados (lambda*matrizThetas)
        		matD - Matriz com valores dos D (gradientes) calculados

  @return       Matriz resultante da soma de P e D
'''
def SomaPD(matP, matD):
    matPD = []
    for i, j in zip(matD, matP):
        s = utils.SomaMatriz(i, np.transpose(j))
        matPD.append(s)
    return matPD

'''
  @Function		AtualizaPesos
  @Brief		Funcao que atualiza os pesos das camadas para proxima
                iteracao da rede

  @Input Parameters

  @param   		lstGradientes - Gradientes atualizados
                gradMomento - Gradientes corrigidos segundo heuristica do momento
        		weightsCamadas - Matriz de pesos das camadas
        		custoJ - Custo total J da rede apos treinamento
        		ALFA - Valor ALFA

  @return       Matriz de pesos atualizada para proxima iteracao
'''
def AtualizaPesos(lstGradientes, gradMomento, weightsCamadas, custoJ, ALFA):
    newWeights = []
    for i, j, k in zip(lstGradientes, weightsCamadas, gradMomento):
        gradJ = np.dot(i, custoJ)
        zij = utils.SomaMatriz(gradJ, k)
        resMomento = np.dot(zij, -ALFA)
        s = utils.SomaMatriz(np.transpose(j), resMomento)
        newWeights.append(np.transpose(s).tolist())

    return newWeights

'''
  @Function		AnalisaCustoRede
  @Brief		Funcao analisa se custo da rede atingiu a taxa tolerável
                de menor ou igual a 10% e não está havendo variação significativa
                entre os custos por rodada, então considera que rede atingiu
                aprendizado máximo e retorna True, sinalizando para terminar o 
                treinamento.

  @Input Parameters

  @param   		jMedioAnt - Custos acumaldos a cada iteração
                jMedio - Custos médio para iteração
                txtControle - Controle para faixa de valores a analisar
  @return       True - finalizar treinamento
'''
def AnalisaCustoRede(jMedioAnt, jMedio, txtControle):
    ret = False

    variacaoCusto = (jMedio - jMedioAnt) / jMedioAnt

    if (jMedio < CUSTO_ACEITAVEL) and (variacaoCusto < VAR_CUSTO_ACEITAVEL):
        ret = True

    # utils.ImprimeInfors('\nAnálise Custo Medio da Rede')
    # print('Rodada ' + txtControle + '\tjAnterior:' + str(jMedioAnt) + ',\tjAtual:' + str(
    #     jMedio) + ',\tvariação custo (%): ' + str(variacaoCusto * 100))
    #
    # jMedioAnt = jMedio

    # print("J MEDIO após lote de iterações: ", J(n)-J(0), J(n)-J(n-1)",
    #                jRede[0]- jRede[len(jRede) - 1],
    #                jRede[len(jRede) - 1] - jRede[len(jRede) - 2])

    return jMedioAnt, ret

'''
  @Function		HeuristicaMomento
  @Brief		Funcao realiza a média dos gradientes regularizados
                anteriormente calculados com o valor calculado na
                iteração atual (método do momento).

  @Input Parameters

  @param   		lstGradRegularAnt - Lista com gradientes acumulados
                gradRegular - Ultimo gradiente acumulado
                 
  @return       Retorna gradiente apos metodo momento e lista com gradientes acumulados
'''
def HeuristicaMomento(lstGradRegularAnt, gradRegular):
    if len(lstGradRegularAnt) > 0:
        gradReg = lstGradRegularAnt[0]
        for i in range(1, len(lstGradRegularAnt)):
            gradReg = utils.SomaMatriz(gradReg, lstGradRegularAnt[i])

        gradReg = GradienteRegularizado(len(lstGradRegularAnt), gradReg)

        for i in range(len(gradReg)):
            gradReg[i] = np.dot(gradReg[i], BETA_MOMENTO)

    else:
        gradReg = gradRegular

    lstGradRegularAnt.append(gradRegular)

    return gradReg, lstGradRegularAnt

'''
  @Function		BackPropagation
  @Brief		Funcao que aplica o algoritmo de backpropagation para
                estimar quanto cada neuronio contribuiu para o erro da
                rede e calcular novos valores para ajuste dos pesos de 
                cada camada da rede afim de diminuir o erro na proxima
                iteracao da rede.

  @Input Parameters

  @param   		weightsCamadas - Matriz de pesos das camadas
        		xy - instancias do conjunto de treinamento
        		treinamento - tipo de treinamento selecionado (estocastico, mini-batch ou batch)

  @return       None
'''
def BackPropagation(weightsCamadas, xy, treinamento):
    jRede = []
    utils.ImprimeInfors('\n========== BACKPROPAGATION =============')
    utils.ImprimeInfors('Calculando erro/custo J da rede')
    n = len(xy)

    ctrl = 0
    custoAceitavel = False
    jMedioAnterior = 0
    matD = []
    firstJTotal = False

    # Impressão da performance custo J por lote de treinamento/teste
    while ((custoAceitavel == False) and (ctrl < MAX_TRY_BP)):
    # while (ctrl < MAX_TRY_BP):
        lstGradRegularAnt = []

        for i in range(len(xy)):
            x = xy[i][:-1][0]
            y = xy[i][-1:][0]
            utils.ImprimeInfors('\nProcessando exemplo de treinamento ', i + 1)
            utils.ImprimeInfors('Propagando entrada ', x)
            lstAtivacao = Propagation(x, weightsCamadas)

            utils.ImprimeInfors('\nSaida predita para o exemplo ' + str(i + 1) + ':',
                                        ["{:.5f}".format(value) for value in lstAtivacao[len(lstAtivacao) - 1]])
            utils.ImprimeInfors('Saida esperada para o exemplo ' + str(i + 1) + ':', ["{:.5f}".format(value) for value in y])

            j = FuncaoCusto(y, lstAtivacao[len(lstAtivacao) - 1])

            utils.ImprimeInfors('J do exemplo ' + str(i + 1) + ':', "{:.5f}".format(j))
            utils.ImprimeInfors('\n')

            jRede.append(j)

            utils.ImprimeInfors('Calculando gradientes com base no exemplo ', str(i + 1) + '\n')
            listDelta = CalculaDelta(y, weightsCamadas, lstAtivacao)
            matGrad = CalculaGradientes(listDelta, lstAtivacao)
            # ImprimeGradiente(matGrad, i)
            if (len(matD) == 0):
                matD = matGrad
            else:
                matD = utils.SomaMatriz(matGrad, matD)

            # Atualiza pesos conforme tamanho do batch para o tipo de treinamento
            if (((i + 1) % treinamento) == 0):
                s = Regularizacao(weightsCamadas, len(jRede))
                j = np.mean(jRede)
                jTotal = j + s

                if (firstJTotal == False):
                    CUSTO_ACEITAVEL = jTotal * 0.10
                    firstJTotal = True

                jMedioAnterior, custoAceitavel = AnalisaCustoRede(jMedioAnterior, jTotal, str(ctrl) + " iteração " + str(i))

                utils.ImprimeInfors("\nJ total do dataset (com regularizacao): ", "{:.5f}".format(jTotal))

                matP = GeraP(weightsCamadas)
                matPD = SomaPD(matP, matD)

                utils.ImprimeInfors('\nDataset completo processado. Calculando gradientes regularizados')

                gradRegular = GradienteRegularizado(n, matPD)
                # ImprimeGradiente(gradRegular)

                gradMomento, lstGradRegularAnt = HeuristicaMomento(lstGradRegularAnt, gradRegular)

                weightsCamadas = AtualizaPesos(gradRegular, gradMomento, weightsCamadas, jTotal, ALFA)
                gLstWeigth.append(weightsCamadas)

                matD = []
                jRede = []

        # # Controle para acumular pesos para impressao J Teste performance
        # if len(lstWeigth) > 0:
        #     gLstWeigth.append(lstWeigth)
        #     lstWeigth = []
        #

        ctrl = ctrl + 1;

    return weightsCamadas

'''
  @Function		GeraConfRedeRandom
  @Brief		Funcao que inicia matriz de pesos com valores randomicamente
                selecionados.

  @Input Parameters

  @param   	    qtNeuronEntrada - Nro de entradas
                qtNeuronSaida - Nro de saidas
                layerWeight - lista onde cada posição representa uma camada e o elemento na posição
                              o total de neuronios da camada
                flagImpr - determina se imprime valores de configuração

  @return       Matriz de pesos aletorios
'''
def GeraConfRedeRandom(qtNeuronEntrada, layerWeight, qtNeuronSaida,flagImpr):
    # utils.ImprimeInfors('\n========== GERA CONFIGURACAO RANDOM PARA REDE =============')
    conf_network = []
    conf_network.append(qtNeuronEntrada)
    for i in range(len(layerWeight)):
        conf_network.append(layerWeight[i])
    conf_network.append(qtNeuronSaida)
    weightsCamadas = []

    if(flagImpr == 0):#para permitir uma unica impressao
            print('\nEstrutura da Rede: ', conf_network)
            print('Alfa: ', str(round(ALFA,5)))
            print('Lambda: ', LBDA)
            print('Beta Momento: ',                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             BETA_MOMENTO)
            print('K-Folds: ', dat.K_FOLDS)
            print('Seed: ', SEED)
            print('\n')
    for i in range(1, len(conf_network)):
        matriz_pesos_random = []
        for j in range(conf_network[i]):
            aux = []
            for k in range(conf_network[i - 1] + 1):
                numberRandom = rd.uniform(-1,1)
                aux.append(numberRandom)
            matriz_pesos_random.append(aux)
        weightsCamadas.append(matriz_pesos_random)
    utils.ImprimeInfors('\nmatriz_pesos_random:', weightsCamadas)
    return weightsCamadas

'''
  @Function		TipoTreinamento
  @Brief		Funcao analisa valor da variavel TREINAMENTO
                para determinar se treinamento será:
                - Estocástico, TREINAMENTO = 1
                - Batch, TREINAMENTO = LEN_DATASET
                - Mini-Batch, TREINAMENTO = valor informado pelo usuário
                  ou 25% do total de amostras quando omitido ou inválido.

  @Input Parameters

  @param   	    lenDataset - Tamanho do conjunto de dados

  @return       Novo valor para TREINAMENTO
'''
def TipoTreinamento(lenDataset, lenTrain, treinamento):
    if (treinamento == 1):
        utils.ImprimeInfors("\nTipo treinamento selecionado: ESTOCÁSTICO\n")
    elif (treinamento == lenDataset):
        treinamento = lenTrain
        utils.ImprimeInfors("\nTipo treinamento selecionado: BATCH\n")
    else:
        utils.ImprimeInfors("\nTipo treinamento selecionado: MINI-BATCH\n")
        if ((treinamento > lenDataset) or (treinamento <= 0)):
            treinamento = int(lenTrain * 0.25)
    return treinamento

'''
  @Function		AvaliaPerformance
  @Brief		Funcao que realiza todo o procedimento do algoritmo. Desde o treinamento,
                ate a avaliacao utilizando o metodo de validacao cruzada

  @Input Parameters

  @param   	    nameDataset - Referencia para arquivo do conjunto de dados
                treinamento - tipo de treinamento selecionado pelo usuario

  @return       Resultados das metricas
'''
def AvaliaPerformance(nameDataset, treinamento):
    print('=========================================')

    print('\nAVALIACAO DE PERFORMANCE')
    dataset = pd.read_csv(nameDataset, delimiter=';')
    conjuntos = dat.ValidacaoCruzada(dataset)
    print('Dataset:', nameDataset)
    lstRedes = []
    recallS = []
    precisionS = []
    fMeasureS = []
    acuraciaS = []

    flagImpr = 0

    for train, test in conjuntos:
        flagWeights = 0
        y_pred = []
        y_true = []
        for xyTrain, xyTest in zip(train, test):
            if (flagWeights == 0):    # configura a rede... com base na entrada e saida
                qtNeuronEntrada = len(xyTrain[0][:-1][0])
                qtNeuronSaida = len(xyTrain[0][-1:][0])
                flagWeights = 1
                weightsCamadas = GeraConfRedeRandom(qtNeuronEntrada, qtNeuronSaida,flagImpr)
                flagImpr = 1
                # print('\nPesos Iniciais Aleatorios')
                # ImprimeGradiente(weightsCamadas, 5)
                treinamento = TipoTreinamento(len(dataset), len(xyTrain), treinamento)

            # realiza o treino da rede
            weightsCamadas = BackPropagation(weightsCamadas, xyTrain, treinamento)

            # testa com base no treino

            # print('\nPesos Finais apos o treinamento')
            # ImprimeGradiente(weightsCamadas, 5)

            utils.ImprimeInfors("REALIZANDO AS PREVISOES DOS EXEMPLOS TESTES")
            for i in range(len(xyTest)):
                x = xyTest[i][:-1][0]
                y = xyTest[i][-1:][0]
                lstAtivacao = Propagation(x, weightsCamadas)
                y_pred.append(lstAtivacao[len(lstAtivacao) - 1])
                y_true.append(y)
                utils.ImprimeInfors('\nSaida predita para o exemplo ' + str(i + 1) + ':',
                                            ["{:.5f}".format(value) for value in lstAtivacao[len(lstAtivacao) - 1]])
                utils.ImprimeInfors('Saida esperada para o exemplo ' + str(i + 1) + ':', ["{:.5f}".format(value) for value in y])

                if (y_true and y_pred):
                    recall, precision, fMeasure, acuracia = dat.MetricasDesempenho(y_true, y_pred)
                    recallS.append(recall)
                    precisionS.append(precision)
                    fMeasureS.append(fMeasure)
                    acuraciaS.append(acuracia)
                    lstRedes.append([weightsCamadas, fMeasure])

    stdA = [np.std(recallS), np.std(precisionS), np.std(fMeasureS), np.std(acuraciaS)]
    meanA = [np.mean(recallS), np.mean(precisionS), np.mean(fMeasureS), np.mean(acuraciaS)]
    result = pd.DataFrame(dict(std=stdA, mean=meanA), index=['Recall', 'Precision', 'fMeasure', 'Acuracia'])
    print(result)
    print('=========================================')
    return result

'''
  @Function		CarregaDados
  @Brief		Funcao que carrega os dados dos arquivos especificados e padroniza no formato
                esperado pelo BackPropagation

  @Input Parameters

  @param   	    

  @return       LBDA,xy,weightsCamadas,rede
'''
def CarregaDados(arq_network, arq_initial_weightsCamadas, nameDataset):
    conf_network = pd.read_csv(arq_network, delimiter='\n', names=['rede'])
    LBDA = conf_network['rede'][0]
    initial_weightsCamadas = pd.read_csv(arq_initial_weightsCamadas, delimiter='\n', names=['pesos'])
    dataset = pd.read_csv(nameDataset, delimiter='\n', names=['conjunto'])
    xy = utils.ConfEntrada(dataset)
    weightsCamadas = utils.ConfweightsCamadas(initial_weightsCamadas)
    rede = conf_network['rede'].tolist()

    return LBDA, xy, weightsCamadas, rede

'''
  @Function		BenchMark
  @Brief		Funcao que computa todos os passos especificados na descricao e printa os valores
                conforme resultados esperados

  @Input Parameters

  @param   	    

  @return       gradRegular
'''
def Benchmark(arq_network, arq_initial_weightsCamadas, nameDataset):
    global LBDA
    LBDA, xy, weightsCamadas, rede = CarregaDados(arq_network, arq_initial_weightsCamadas, nameDataset)

    utils.ImprimeInfors('\nParametro de regularização lambda= %.5f' % LBDA)
    utils.ImprimeInfors('Inicializando a rede com a seguinte estrutura de neuronios por camada: ',
                                ["{:.0f}".format(value) for value in rede[1:]])

    for k in range(len(weightsCamadas)):
        utils.ImprimeInfors("\nTheta" + str(k + 1) + " inicial (pesos de cada neuronio, incluindo bias, armazenados nas linhas):")
        for i in range(len(weightsCamadas[k])):
            utils.ImprimeInfors(["{:.5f}".format(value) for value in weightsCamadas[k][i]])
        # utils.ImprimeInfors('\nConjunto de Treinamento')

    for i in range(len(xy)):
        utils.ImprimeInfors('Exemplo ', i + 1)
        l = 0
        for j in range(len(xy[i])):
            if(l == 0):
                l=1
                utils.ImprimeInfors('\tx:',["{:.5f}".format(value) for value in xy[i][j]])
            else:
                utils.ImprimeInfors('\ty:',["{:.5f}".format(value) for value in xy[i][j]])
    utils.ImprimeInfors('\n--------------------------------------------\n')

    jRede = []
    xyC = utils.CopiaLista(xy)
    weightsCamadasC = utils.CopiaLista(weightsCamadas)

    utils.ImprimeInfors('Calculando erro/custo J da rede')
    for i in range(len(xyC)):
        utils.ImprimeInfors('Processando exemplo de treinamento ', i+1)
        utils.ImprimeInfors('Propagando entrada ', xyC[i][:-1][0], '*********')
        lstAtivacao = Propagation(xyC[i][:-1][0], weightsCamadas)
        utils.ImprimeInfors('\tSaida predita para o exemplo ' + str(i + 1) + ':',
                                    ["{:.5f}".format(value) for value in lstAtivacao[len(lstAtivacao) - 1]])
        utils.ImprimeInfors(
            '\tSaida esperada para o exemplo ' + str(i + 1) + ':', ["{:.5f}".format(value) for value in xy[i][-1:][0]])
        j = FuncaoCusto(xyC[i][-1:][0], lstAtivacao[len(lstAtivacao) - 1])
        utils.ImprimeInfors('\tJ do exemplo ' + str(i + 1) + ':', j)
        utils.ImprimeInfors('\n')
        jRede.append(j)
    s = Regularizacao(weightsCamadas, len(jRede))
    J = np.mean(jRede)
    utils.ImprimeInfors('J total do dataset (com regularizacao):', J + s)
    n = len(xy)
    utils.ImprimeInfors('\n--------------------------------------------')
    utils.ImprimeInfors('Rodando backpropagation')
    for i in range(n):
        x = xy[i][:-1][0]
        y = xy[i][-1:][0]
        lstAtivacao = []
        a = AdicionaBias(x)
        lstAtivacao.append(a)
        for j in range(len(weightsCamadasC)):
            matrixweightsCamadas = weightsCamadasC[j]
            z = np.matmul(matrixweightsCamadas, a)
            a = AtivacaoNeuronio(z.tolist())
            lstAtivacao.append(a)

            if (j == (len(weightsCamadasC) - 1)):
                break
            a = AdicionaBias(a)

        utils.ImprimeInfors('\nCalculando gradientes com base no exemplo ', i + 1)

        listDelta = CalculaDelta(y, weightsCamadasC, lstAtivacao)
        matGrad = CalculaGradientes(listDelta, lstAtivacao)
        ImprimeGradiente(matGrad, i)
        if (i == 0):
            matD = matGrad
        else:
            matD = utils.SomaMatriz(matGrad, matD)
    matP = GeraP(weightsCamadasC)
    matPD = SomaPD(matP, matD)
    utils.ImprimeInfors('\nDataset completo processado. Calculando gradientes regularizados')
    gradRegular = GradienteRegularizado(n, matPD)

    ImprimeGradiente(gradRegular)

    return gradRegular