# neural_networks

Para executar:
		
	BACKPROPAGATION
	
	Rede 1: 
	python3 back_propagation.py config_files/network1.txt config_files/initial_weights1.txt config_files/dataset1.txt >> BenchMarkRede1.txt
	
	Rede 2:
	python3 back_propagation.py config_files/network2.txt config_files/initial_weights2.txt config_files/dataset2.txt >> BenchMarkRede2.txt
		
	
	VERIFICACAO NUMERICA:
	
	Rede 1:
	python3 numerical_verification.py config_files/network1.txt config_files/initial_weights1.txt config_files/dataset1.txt >> NumericalRede1.txt
	
	Rede2: 
	python3 numerical_verification.py config_files/network2.txt config_files/initial_weights2.txt config_files/dataset2.txt >> NumericalRede2.txt
	
	OBS: Note, que executando dessa forma, ira gerar arquivos de saida com os resultados conforme especificado.
	
	NEURAL NETWORK - Completa
	
	Exemplo utilizando o padrao definido para entrada na rede:
		
	python3 neural_network.py datasets/pima-indians-diabetes.csv config_files/network_pima.txt config_files/initial_weights_pima.txt 
	
	Exemplo utilizando pesos gerados automaticamente e demais configuracoes. Deve-se, habilitar e configurar
	as variaveis no arquivo neural_network conforme o desejado.
			bp.MAX_TRY_BP = 10000
			bp.TREINAMENTO = 0
			bp.CUSTO_ACEITAVEL = 0.1
			bp.VAR_CUSTO_ACEITAVEL = 0.1
			bp.BETA_MOMENTO = 0.9
			bp.ALFA = 0.01
			bp.LBDA = 0.250
			dat.K_FOLDS = 10
			layerWeight = [2,2]
	E para a execucao, de cada dataset: 
		
		python3 neural_network.py datasets/wine_data.csv
		python3 neural_network.py datasets/ionosphere_data.csv
		python3 neural_network.py datasets/wdbc_data.csv
		python3 neural_network.py datasets/irisDataset.csv
		
	
